<%-- 
    Document   : listadoEstudiantes.jsp
    Created on : 01-jun-2021, 20:24:08
    Author     : GaCTuS
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.proyectoevaluacion2.entity.Estudiantes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%  List<Estudiantes> listadoEstudiantes = (List<Estudiantes>)request.getAttribute("listadoEstudiantes"); 
    if (listadoEstudiantes == null){
        response.sendRedirect("index.jsp");
    }
    Iterator<Estudiantes> itEstudiantes = listadoEstudiantes.iterator();%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mantenedor Estudiantes</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
        <link rel="stylesheet" href="css/styles.min.css">
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
        <link href="css/font-awesome.min.css" rel="stylesheet" />
        <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
    <body  text=”#3399FF”   >
        <form id="frmEstudiantes" name="frmEstudiantes" style="font-family:Quicksand, sans-serif;background-color:rgba(230,230,230,0.73);width:1000px;padding:40px;" method="post">
            <table align="center" width="950">
                <tr>
                    <td>
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <div class="row">
                                    <div class="col col-lg-9">
                                        <h2 class="titulosCabeceras">Mantenedor de Estudiantes</h2>
                                    </div>
                                    <div class="col col-lg-2 text-right">
                                        <button type="button" id="btnNuevo" name="btnNuevo" class="botonLimpiar" data-toggle="modal" data-target="#modalIngreso"><span class="textosNormal"><span class="fa fa-user-plus"></span>&nbsp;Nuevo</span></button>
                                        <input type="hidden" id="hddRut" name="hddRut">
                                    </div>
                                    <div class="col col-lg-1"></div>
                                </div>
                            </div>
                            <div class="card-body">
                                <table border="0" cellpadding="2" cellspacing="1" align="center" border="0" width="900" class="table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>Nombre</th>
                                            <th>rut</th>
                                            <th>Edad</th>
                                            <th>Dirección</th>
                                            <th>Teléfono</th>
                                            <th>Edición</th>
                                        </tr>
                                    </thead>
<%                          while (itEstudiantes.hasNext()){
                                Estudiantes listaEstudiantes = itEstudiantes.next(); %>                                    
                                    <tr>
                                        <td class="textosOk"><%= listaEstudiantes.getRut() %></td>
                                        <td class="textosOk"><%= listaEstudiantes.getNombre() %></td>
                                        <td class="textosOk"><%= listaEstudiantes.getEdad() %></td>
                                        <td class="textosOk"><%= listaEstudiantes.getDireccion() %></td>
                                        <td class="textosOk">+<%= listaEstudiantes.getTelefono() %></td>
                                        <td><button type="button" id="btnEditar" name="btnEditar" class="botonEdicion" alt="Editar" onclick="fnEditarEstudiante('<%= listaEstudiantes.getRut() %>')"><span class="textosOk"><span class="fa fa-edit"></span></span></button>&nbsp;<button type="button" id="btnEditar" name="btnEditar" class="botonEdicion" alt="Eliminar" onclick="fnEliminaEstudiante('<%= listaEstudiantes.getRut() %>');"><span class="textosError"><span class="fa fa-trash"></span></span></button></td>
                                    </tr>
<%                          }   %>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr >
                    <td align="center">
                        <button type="button" id="btnHome" name="btnHome" class="botonCancelar" onclick="fnVolverHome();"><span><span class="fa fa-home"></span>&nbsp;Inicio</span></button>
                    </td>
                </tr>  
            </table>
        </form>
        <div class="modal fade" id="modalIngreso" tabindex="-1" role="dialog" aria-labelledby="lblTitulo" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="lblTitulo">Nuevo Estudiante</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="frmIngresaEstudiantes" id="frmIngresaEstudiantes" method="post" action="RegistrarController">
                            <table align="center" width="550" cellpadding="2" cellspacing="1">
                                <tr>
                                    <td align="right" class="textosNormal" width="200">Nombre Estudiante:</td>
                                    <td align="left" width="350" class="textosNormal"><input type="text" name="txtNombre" id="txtNombre" placeholder="Luis Garcés Sepulveda" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" onblur="this.value = this.value.toUpperCase();" tabindex="1"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="textosNormal" width="200">Rut Estudiante:</td>
                                    <td align="left" width="350" class="textosNormal"><input type="text" name="txtRut" id="txtRut" placeholder="11111111-1" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" tabindex="2"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="textosNormal">Edad Estudiante:</td>
                                    <td align="left" class="textosNormal"><input type="text" name="txtEdad" id="txtEdad" placeholder="18" class="textosCajas" style="width: 50px;" maxlength="2" onfocus="fnLimpiarErrores();" tabindex="3"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="textosNormal">Dirección Estudiante:</td>
                                    <td align="left" class="textosNormal"><input type="text" name="txtDireccion" id="txtDireccion" placeholder="Erasmo Escala 1875" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" onblur="this.value = this.value.toUpperCase();" tabindex="4"></td>
                                </tr>
                                <tr>
                                    <td align="right" class="textosNormal">Teléfono Estudiante:</td>
                                    <td align="left" class="textosNormal"><input type="text" name="txtTelefono" id="txtTelefono" placeholder="5699555555" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" maxlength="11" tabindex="5"></td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <!-- onclick="fnValidaIngresoEstudiante();" -->
                                        <button type="button" class="botonAccion"  tabindex="6" onclick="fnValidaIngresoEstudiante();"><span><span class="fa fa-save"></span>&nbsp;Guardar</span></button>
                                        <button type="button" class="botonLimpiar" onclick="fnLimpiaIngresoEstudiante();" tabindex="7"><span>Limpiar&nbsp;</span><span class="fa fa-eraser"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" id="errores" class="textosError"></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <input type="button" value="Cerrar" class="botonCancelar" data-dismiss="modal" tabindex="10">
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
