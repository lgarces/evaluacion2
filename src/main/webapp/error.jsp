<%-- 
    Document   : error
    Created on : 02-jun-2021, 11:13:05
    Author     : GaCTuS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error de Ingreso</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
        <link rel="stylesheet" href="css/styles.min.css">
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
        <link href="css/font-awesome.min.css" rel="stylesheet" />
        <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
    <body>
        <form id="frmEstudiantes" name="frmEstudiantes" method="post" action="EditarController" style="font-family:Quicksand, sans-serif;background-color:rgba(230,230,230,0.73);width:1000px;padding:40px;">
            <div class="card">
                <div class="card card-header bg-dark">
                    <h2><span class="fa fa-exclamation-circle"></span>&nbsp;Ha ocurrido un Error!</h2>
                </div>
                <div class="card card-body">
                    <table align="center" width="550" cellpadding="2" cellspacing="1" class="table">
                        <tbody>
                            <tr>
                                <td align="center" class="textosOkBold">
                                    Ha ocurrido un error con el rut ingresado, debido a que este ya existe en nuestras bases.
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <button type="button" id="btnVolver" name="btnVolver" class="botonCancelar" onclick="fnVolverListado();"><span>Volver al listado&nbsp;<span class="fa fa-mail-reply"></span></span></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
