<%-- 
    Document   : index.jsp
    Created on : 01-jun-2021, 20:23:55
    Author     : GaCTuS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Estudiantes</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
        <link rel="stylesheet" href="css/styles.min.css">
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
        <link href="css/font-awesome.min.css" rel="stylesheet" />
        <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
    <body>
        <form id="frmEstudiantes" name="frmEstudiantes" style="font-family:Quicksand, sans-serif;background-color:rgba(230,230,230,0.73);width:1000px;padding:40px;">
            <table>
                <thead>
                <th><h2>Home Estudiantes</h2></th>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <table class="table">
                                <td>
                                    <img src="imagenes/imgEstudiantes.jpg" width="100%" class="imagenCentral">
                                </td>
                                <td class="textosOkBold">
                                    Bienvenido/a al sistema de mantención de estudiates, mediante un patron CRUD, a continuación encontrará el acceso al sistema.<br><br>
                                    <table>
                                        <tr>
                                            <td>
                                                <button type="button" id="btnMantenedor" name="btnMantendor" class="botonLimpiar" onclick="fnIngresaMantenedor();"><span class="textosNormal"><span class="fa fa-address-card"></span>&nbsp;Ingresar</span></button>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </table> 
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </body>
</html>
