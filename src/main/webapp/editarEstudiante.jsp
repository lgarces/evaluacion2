<%-- 
    Document   : editarEstudiante
    Created on : 02-jun-2021, 0:26:43
    Author     : GaCTuS
--%>
<%@page import="cl.proyectoevaluacion2.entity.Estudiantes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%  Estudiantes datosEstudiante = (Estudiantes) request.getAttribute("datosEstudiante");
    if (datosEstudiante == null) {
        response.sendRedirect("ListarController");
    }%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Datos Estudiante</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand">
        <link rel="stylesheet" href="css/styles.min.css">
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
        <link href="css/font-awesome.min.css" rel="stylesheet" />
        <script type="text/javascript" src="js/validaciones.js"></script>
    </head>
    <body>
        <form id="frmEstudiantes" name="frmEstudiantes" method="post" action="EditarController" style="font-family:Quicksand, sans-serif;background-color:rgba(230,230,230,0.73);width:1000px;padding:40px;">
            <div class="card">
                <div class="card card-header bg-dark">
                    <h2>Editar Estudiante</h2>
                </div>
                <div class="card card-body">
                    <table align="center" width="550" cellpadding="2" cellspacing="1" class="table">
                        <tr>
                            <td align="right" class="textosOk" width="200">Nombre Estudiante:</td>
                            <td align="left" width="350" class="textosNormal"><input type="text" name="txtNombre" id="txtNombre" placeholder="Luis Garcés Sepulveda" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" onblur="this.value = this.value.toUpperCase();" tabindex="1" value="<%= datosEstudiante.getNombre()%>"></td>
                        </tr>
                        <tr>
                            <td align="right" class="textosOk" width="200">Rut Estudiante:</td>
                            <td align="left" width="350" class="textosNormal"><input type="text" name="txtRut" id="txtRut" placeholder="11111111-1" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" tabindex="2" readonly="readonly" value="<%= datosEstudiante.getRut()%>"></td>
                        </tr>
                        <tr>
                            <td align="right" class="textosOk">Edad Estudiante:</td>
                            <td align="left" class="textosNormal"><input type="text" name="txtEdad" id="txtEdad" placeholder="18" class="textosCajas" style="width: 50px;" maxlength="2" onfocus="fnLimpiarErrores();" value="<%= datosEstudiante.getEdad()%>" tabindex="3"></td>
                        </tr>
                        <tr>
                            <td align="right" class="textosOk">Dirección Estudiante:</td>
                            <td align="left" class="textosNormal"><input type="text" name="txtDireccion" id="txtDireccion" placeholder="Erasmo Escala 1875" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" onblur="this.value = this.value.toUpperCase();" tabindex="4" value="<%= datosEstudiante.getDireccion()%>"></td>
                        </tr>
                        <tr>
                            <td align="right" class="textosOk">Teléfono Estudiante:</td>
                            <td align="left" class="textosNormal"><input type="text" name="txtTelefono" id="txtTelefono" placeholder="5699555555" class="textosCajas" style="width: 220px;" onfocus="fnLimpiarErrores();" maxlength="11" tabindex="5" value="<%= datosEstudiante.getTelefono()%>"></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <!-- onclick="fnValidaIngresoEstudiante();" -->
                                <button type="button" class="botonAccion"  tabindex="6" onclick="fnValidaEdicionEstudiante();"><span><span class="fa fa-edit"></span>&nbsp;Editar</span></button>
                                <button type="button" class="botonLimpiar" tabindex="7" onclick="fnVolverListado();"><span>Volver&nbsp;<span class="fa fa-mail-reply"></span></span></button>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" id="errores" class="textosError"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>
