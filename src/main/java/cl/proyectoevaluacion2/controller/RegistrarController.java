/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.proyectoevaluacion2.controller;

import cl.proyectoevaluacion2.dao.EstudiantesJpaController;
import cl.proyectoevaluacion2.entity.Estudiantes;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author GaCTuS
 */
@WebServlet(name = "RegistrarController", urlPatterns = {"/RegistrarController"})
public class RegistrarController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegistrarController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegistrarController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String rutEstudiante;
        String nombreEstudiante;
        int edadEstudiante;
        String direccionEstudiante;
        String telefonoEstudiante;
        if (request.getParameter("txtRut") == "") {
            rutEstudiante = "";
        } else {
            rutEstudiante = request.getParameter("txtRut");
        }
        if (request.getParameter("txtNombre") == "") {
            nombreEstudiante = "";
        } else {
            nombreEstudiante = request.getParameter("txtNombre");
        }
        if (request.getParameter("txtEdad") == "") {
            edadEstudiante = 0;
        } else {
            edadEstudiante = Integer.parseInt(request.getParameter("txtEdad"));
        }
        if (request.getParameter("txtDireccion") == "") {
            direccionEstudiante = "";
        } else {
            direccionEstudiante = request.getParameter("txtDireccion");
        }
        if (request.getParameter("txtTelefono") == "") {
            telefonoEstudiante = "";
        } else {
            telefonoEstudiante = request.getParameter("txtTelefono");
        }
        Estudiantes datosEstudiante = new Estudiantes();
        datosEstudiante.setRut(rutEstudiante);
        datosEstudiante.setNombre(nombreEstudiante);
        datosEstudiante.setEdad(edadEstudiante);
        datosEstudiante.setDireccion(direccionEstudiante);
        datosEstudiante.setTelefono(telefonoEstudiante);
        
        EstudiantesJpaController daoEstudiantes = new EstudiantesJpaController();
        Estudiantes validaEstudiante = daoEstudiantes.findEstudiantes(rutEstudiante);
        if (validaEstudiante == null){
            try{
                daoEstudiantes.create(datosEstudiante);
            }catch(Exception ex){
                System.out.print("Ha ocurrido el siguiente error:" + ex.toString());
                //Logger.getLogger(RegistrarController.class.getName()).log(Level.SEVERE, null, ex);
            }
            response.sendRedirect("ListarController");
        }else{
            response.sendRedirect("error.jsp");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
